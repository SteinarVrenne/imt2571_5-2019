<?php
/**
  * Class for accessing DOM data representation of the contents of a Disney.xml
  * file
  */
class Disney
{
    /**
      * The object model holding the content of the XML file.
      * @var DOMDocument
      */
    protected $doc;

    /**
      * An XPath object that simplifies the use of XPath for finding nodes.
      * @var DOMXPath
      */
    protected $xpath;

    /**
      * param String $url The URL of the Disney XML file
      */
    public function __construct($url)
    {
        $this->doc = new DOMDocument();
        $this->doc->load($url);
        $this->xpath = new DOMXPath($this->doc);
    }

    /**
      * Creates an array structure listing all actors and the roles they have
      * played in various movies.
      * returns Array The function returns an array of arrays. The keys of they
      *               "outer" associative array are the names of the actors.
      *                The values are numeric arrays where each array lists
      *                key information about the roles that the actor has
      *                played. The elments of the "inner" arrays are string
      *                formatted this way:
      *               'As <role name> in <movie name> (movie year)' - such as:
      *               array(
      *               "Robert Downey Jr." => array(
      *                  "As Tony Stark in Iron Man (2008)",
      *                  "As Tony Stark in Spider-Man: Homecoming (2017)",
      *                  "As Tony Stark in Avengers: Infinity War (2018)",
      *                  "As Tony Stark in Avengers: Endgame (2019)"),
      *               "Terrence Howard" => array(
      *                  "As Rhodey in Iron Man (2008)")
      *               )
      */
    public function getActorStatistics()
    {
        $result = array();

        $numOfActors = $this->xpath->query("//Actors/Actor");

        foreach($numOfActors as $actor){

          $result[$actor->getElementsByTagName("Name")[0]->nodeValue] = array();
          $roles = $this->xpath->query("//Subsidiaries/Subsidiary/Movie/Cast/Role[@actor = '{$actor->getAttribute("id")}']");

          foreach ($roles as $role) {
            $parentNode = $role->parentNode->parentNode;
            array_push($result[$actor->getElementsByTagName("Name")[0]->nodeValue], "As " . $role->getAttribute("name") . " in " . $parentNode->getElementsByTagName("Name")[0]->nodeValue . " (". $parentNode->getElementsByTagName("Year")[0]->nodeValue . ")");
          }
      }

        //To do:
        // Implement functionality as specified
        // DONE

        // codecept_debug($result);
        return $result;
    }

    /**
      * Removes Actor elements from the $doc object for Actors that have not
      * played in any of the movies in $doc - i.e., their id's do not appear
      * in any of the Movie/Cast/Role/@actor attributes in $doc.
      */
    public function removeUnreferencedActors()
    {
        //To do:
        // Implement functionality as specified
        // DONE

        $actors = $this->xpath->query("//Actors/Actor");

        foreach ($actors as $actor) {
          $roles = $this->xpath->query("//Subsidiaries/Subsidiary/Movie/Cast/Role[@actor = '{$actor->getAttribute("id")}']");

          if ($roles->length == 0) {
            // codecept_debug($actor->nodeValue);
            $parentNode = $actor->parentNode;
            $parentNode->removeChild($actor);
          }
        }
    }


    /**
      * Adds a new role to a movie in the $doc object.
      * @param String $subsidiaryId The id of the Disney subsidiary
      * @param String $movieName    The name of the movie of the new role
      * @param Integer $movieYear   The production year of the given movie
      * @param String $roleName     The name of the role to be added
      * @param String $roleActor    The id of the actor playing the role
      * @param String $roleAlias    The role's alias (optional)
      */
    public function addRole($subsidiaryId, $movieName, $movieYear, $roleName,
                            $roleActor, $roleAlias = null)
    {
        //To do:
        // Implement functionality as specified
        // DONE

        $subsidiary = $this->xpath->query("//Subsidiaries/Subsidiary[@id = '{$subsidiaryId}']/Movie[Name = '{$movieName}' and Year = '{$movieYear}']");

        // Check if we find a corresponding movie to add a role
        if ($subsidiary->length != 0) {
          $cast = $subsidiary[0]->getElementsByTagName("Cast")[0];

          // $newRole = $cast->getElementsByTagName("Role")[0]->cloneNode(true);

          $newRole = $this->doc->createElement("Role");

          $newRole->setAttribute("name", $roleName);
          $newRole->setAttribute("actor", $roleActor);
          $newRole->setAttribute("alias", $roleAlias);

          $cast->appendChild($newRole);

          // codecept_debug($cast->getElementsByTagName("Role"));
          // codecept_debug($newRole->getAttribute("name"));
        }
    }
}
?>
